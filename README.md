# GeCoCoo Benchmarking on LiDO3

Tools for executing benchmarks on LiDO3. Results stored in this repository can be viewed at https://gecocoo.gitlab.io/gecocoo-benchmark/.

## Prerequisites

The script needs to be able to clone the gecocoo repository on the LiDO3 host.
It is therefore necessary to authenticate against GitLab.
This is possible via two options:

1. using SSH agent forwarding and thereby reusing the SSH key on your host
2. using a personal GitLab access token

Generate your personal GitLab access token at https://gitlab.com/-/profile/personal_access_tokens.
Make sure to check the scopes `read_repository` and `write_repository`.

## Setup on LiDO3

1.  Load the required modules:

    ```bash
    module load python/3.7.7
    module load git/2.28.0
    ```

    It's best to put them in your `.bashrc` for the future.

2.  Clone this repository into home directory:

    ```bash
    # Using ssh agent forwarding:
    git clone git@gitlab.com:gecocoo/gecocoo-benchmark.git
    # Using your access token
    git clone https://oauth2:<GITLAB TOKEN HERE>@gitlab.com/gecocoo/gecocoo-benchmark.git
    ```

3.  Set the URL to clone the gecocoo repository by:

    ```bash
    # Using ssh agent forwarding:
    export GECOCOO_CLONE_URL=git@gitlab.com:gecocoo/gecocoo.git
    # Using your access token
    export GECOCOO_CLONE_URL=https://oauth2:<GITLAB TOKEN HERE>@gitlab.com/gecocoo/gecocoo.git
    ```

    Possibly save this in a file, so you don't have to type this every time.

3.  Set your email address:

    ```bash
    export LIDO3_MAIL=max.mustermann@tu-dortmund.de
    ```

    Possibly save this in a file, so you don't have to type this every time.

## Running benchmarks

The repository contains a script for running the benchmarks for a specific revision of the gecocoo repository.

```bash
# For the latest master commit
./benchmark.py origin/master
# For some specific commit
./benchmark.py a3c21f4843075ecd483f1eeeb15a24f086a9bd46
```

This will do the following steps for you:

1. Clone the gecocoo repository and checkout the commit
2. Install the dependencies using `pip`
3. Schedule a SLURM job to run all the benchmarks
4. After finishing, copy the results from the job
5. TODO: Cleanup after you (currently disabled for debugging purposes)

As the benchmarks might take a while to finish, it might be a good idea to use `tmux` in case your SSH session gets closed.

## Adding Results

After running the benchmarks as described above and went well, there should be a new directory named after commit and the timestamp the benchmark was started at in the directory `results` of your checked out version of this repository.
You can then commit this directory with all its contents for others to see the results.
The results will be available at https://gecocoo.gitlab.io/gecocoo-benchmark/.
