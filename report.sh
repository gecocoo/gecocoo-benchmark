#!/usr/bin/env bash

set -e

BENCHMARK_DATA="results/076f6b8dee45b7d8278b8c11e7b7c31f610d325e/2021-03-06T20_04_12.073701/results"

python3 plot.py --op inserting --usetex --type pgf --width=17 --height=10 --disabled True $BENCHMARK_DATA $1
python3 plot.py --op searching --usetex --type pgf --width=17 --height=10 --disabled True $BENCHMARK_DATA $1
