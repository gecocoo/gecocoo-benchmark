import os
import re
import json
from dataclasses import dataclass
from typing import List, Literal, Union
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as mpatches
import numpy as np
import math
import sys
import argparse

COLORS = ["#1b9e77", "#d95f02", "#7570b3"]
MARKERS = ["o", "s", "x"]


def cm2inch(cms):
    cm_per_inch = 2.54
    return tuple(i / cm_per_inch for i in cms)


@dataclass
class ResultData:
    operation: Literal["inserting", "searching"]
    mode: Literal["random-walk", "cost-optimal"]
    k: int
    hash_count: int
    bucket_size: int
    load_factor: float
    thread_count: int
    key_count: int
    search_hitting: int
    search_missing: int
    min: float
    max: float
    mean: float
    stddev: float
    rounds: int
    median: float
    iqr: float
    q1: float
    q3: float
    iqr_outliers: int
    stddev_outliers: int
    outliers: str
    ld15iqr: float
    hd15iqr: float
    ops: float
    total: float
    data: List[float]
    iterations: int

@dataclass
class StorageResult:
    mode: Literal["packed", "buckte", "fast"]
    concurrency: bool
    operation: Literal["inserting", "searching"]
    min: float
    max: float
    mean: float
    stddev: float
    rounds: int
    median: float
    iqr: float
    q1: float
    q3: float
    iqr_outliers: int
    stddev_outliers: int
    outliers: str
    ld15iqr: float
    hd15iqr: float
    ops: float
    total: float
    data: List[float]
    iterations: int

def collect(results_dir: str):
    pattern = r"^config_\d+-results-.+.json"
    files = list(filter(lambda f: re.match(pattern, f), os.listdir(results_dir)))
    json_results = []

    for filename in files:
        with open(os.path.join(results_dir, filename)) as file:
            json_results.append(json.load(file))

    results: List[ResultData] = []
    for r in json_results:
        for b in r["benchmarks"]:
            config_str = b["extra_info"]["config"]
            config = json.loads(config_str)
            thread_count = b["extra_info"]["thread_count"]
            key_count = b["extra_info"]["key_count"]
            search_missing = b["extra_info"].get("search_missing", None)

            search_hitting = b["extra_info"].get("search_hitting", None)
            if search_hitting is None:
                search_hitting = b["extra_info"].get(
                    "search_hittíng", None
                )  # Watch out for the "í"

            if "random_walk" in b["fullname"]:
                mode = "random-walk"
            else:
                mode = "cost-optimal"

            result = ResultData(
                operation=b["group"],
                mode=mode,
                k=config["k"],
                hash_count=len(config["hash_functions"]),
                bucket_size=config["bucket_size"],
                load_factor=round(
                    key_count / (config["bucket_count"] * config["bucket_size"]), 2
                ),
                thread_count=thread_count,
                key_count=key_count,
                search_hitting=search_hitting,
                search_missing=search_missing,
                **b["stats"],
            )

            results.append(result)
    return pd.DataFrame(results)

def collect_storage(results_dir: str):
    pattern = r"storage_modes.json"
    files = list(filter(lambda f: re.match(pattern, f), os.listdir(results_dir)))
    json_results = []

    for filename in files:
        with open(os.path.join(results_dir, filename)) as file:
            json_results.append(json.load(file))

    results: List[ResultData] = []
    for r in json_results:
        for b in r["benchmarks"]:
            result = StorageResult(
                operation="inserting" if b["group"].find("insert") != -1 else "searching",
                mode=re.sub("StorageMode\.","",re.search("StorageMode\.\w*[, ]",b["name"]).group())[:-1].lower(),
                concurrency=b["name"].find("concurrent = False") > -1,
                **b["stats"],
            )

            results.append(result)
    return pd.DataFrame(results)


def plot(df: pd.DataFrame, title: str, max_time: float, size):
    fig, ax = plt.subplots()
    bucket_sizes = list(sorted(df["bucket_size"].unique()))
    thread_counts = list(sorted(df["thread_count"].unique()))
    bucket_size_pos = dict(
        zip(sorted(bucket_sizes), [i * 11 for i in range(len(bucket_sizes))],)
    )
    hash_counts = list(sorted(df["hash_count"].unique()))

    for i, hash_count_group in enumerate(df.groupby("hash_count")):
        _, by_hash_count = hash_count_group
        color = COLORS[i]
        for h, thread_count_group in enumerate(by_hash_count.groupby("thread_count")):
            _, by_thread_count = thread_count_group
            y = list([time for r in by_thread_count.itertuples() for time in r.data])
            x = np.array(
                [
                    bucket_size_pos[r.bucket_size] + i * 3 + h
                    for r in by_thread_count.itertuples()
                    for _ in r.data
                ]
            )

            ax.scatter(x=x, y=y, c=color, marker=MARKERS[h], alpha=0.5)

    ax.legend(
        handles=[
            *[
                mlines.Line2D(
                    [],
                    [],
                    marker=marker,
                    linestyle="None",
                    markersize=10,
                    label=f"tc = {thread_count}",
                )
                for thread_count, marker in zip(thread_counts, MARKERS)
            ],
            *[
                mpatches.Patch(color=color, label=f"hc = {hash_count}")
                for hash_count, color in zip(hash_counts, COLORS)
            ],
        ],
        bbox_to_anchor=(1.02, 1),
        loc="upper left",
    )

    ax.set_xticks(np.array(list(bucket_size_pos.values())) + 4.5)
    ax.set_xticklabels(np.array(list(bucket_size_pos.keys())))
    ax.set_ylim(0, max_time * 1.1)
    ax.set_title(title)
    ax.set_ylabel("time (s)")
    ax.set_xlabel("bucket size")

    fig.set_size_inches(*size)
    fig.subplots_adjust(right=0.8)

    return fig


def plot_single_thread(df: pd.DataFrame, title: str, max_time: float, size):
    fig, ax = plt.subplots()
    bucket_sizes = list(df["bucket_size"].unique())
    bucket_size_pos = dict(
        zip(sorted(bucket_sizes), [i * 11 for i in range(len(bucket_sizes))],)
    )
    load_factors = list(sorted(df["load_factor"].unique()))
    hash_counts = list(sorted(df["hash_count"].unique()))

    assert len(list(df["thread_count"].unique())) == 1

    for i, hash_count_group in enumerate(df.groupby("hash_count")):
        _, by_hash_count = hash_count_group
        color = COLORS[i]
        for h, loadfactor_group in enumerate(by_hash_count.groupby("load_factor")):
            _, by_load_factor = loadfactor_group
            y = list([time for r in by_load_factor.itertuples() for time in r.data])
            x = np.array(
                [
                    bucket_size_pos[r.bucket_size] + i * 3 + h
                    for r in by_load_factor.itertuples()
                    for _ in r.data
                ]
            )
            multiple_results = all(
                [len(r.data) > 1 for r in by_load_factor.itertuples()]
            )

            ax.scatter(
                x=x,
                y=y,
                c=color,
                marker=MARKERS[h],
                alpha=0.4 if multiple_results else 1,
            )

    ax.legend(
        handles=[
            *[
                mlines.Line2D(
                    [],
                    [],
                    marker=marker,
                    linestyle="None",
                    markersize=10,
                    label=f"lf = {load_factor}",
                )
                for load_factor, marker in zip(load_factors, MARKERS)
            ],
            *[
                mpatches.Patch(color=color, label=f"hc = {hash_count}")
                for hash_count, color in zip(hash_counts, COLORS)
            ],
        ],
        bbox_to_anchor=(1.02, 1),
        loc="upper left",
    )

    ax.set_xticks(np.array(list(bucket_size_pos.values())) + 4.5)
    ax.set_xticklabels(np.array(list(bucket_size_pos.keys())))
    ax.set_ylim(0, max_time * 1.1)
    ax.set_title(title)
    ax.set_ylabel("time (s)")
    ax.set_xlabel("bucket size")

    fig.set_size_inches(*size)
    fig.tight_layout()
    fig.subplots_adjust(right=0.8)

    return fig

def plot_storage(df: pd.DataFrame, title: str, max_time: float, size):
    fig, ax = plt.subplots()
    modes = list(df["mode"].unique())
    concurrent = list(df["concurrency"].unique())

    for i, mode_group in enumerate(df.groupby("mode",sort=False)):
        _, by_mode_group = mode_group
        for h, concurrent_group in enumerate(by_mode_group.groupby("concurrency")):
            _, by_concurrent = concurrent_group
            y = list([time for r in by_concurrent.itertuples() for time in r.data])
            x = np.array(
                [
                    r.mode
                    for r in by_concurrent.itertuples()
                    for _ in r.data
                ]
            )
            multiple_results = all(
                [len(r.data) > 1 for r in by_concurrent.itertuples()]
            )

            ax.scatter(
                x=x,
                y=y,
                c=COLORS[h],
                marker=MARKERS[0],
                alpha=0.4 if multiple_results else 1,
            )

    ax.legend(
        handles=[
            *[
                mpatches.Patch(color=color, label=f"concurrent = {con}")
                for con, color in zip(concurrent, COLORS)
            ],
        ],
        bbox_to_anchor=(1.02, 1),
        loc="upper left",
    )

    # ax.set_xticks(np.array(list(bucket_size_pos.values())) + 4.5)
    # ax.set_xticklabels(modes)
    ax.set_ylim(0, max_time * 1.1)
    ax.set_title(title)
    ax.set_ylabel("time (s)")
    ax.set_xlabel("storage modes")

    fig.set_size_inches(*size)
    fig.tight_layout()
    fig.subplots_adjust(right=0.8)

    return fig

def render_inserting_random_walk(df, output_dir: str, type: str, dpi: int, size, disabled:bool):
    random_walk_df = df[
        (df["operation"] == "inserting")
        & (df["mode"] == "random-walk")
        & (df["thread_count"] > 1)
    ]
    assert len(df[df["operation"] == "inserting"]["key_count"].unique()) == 1
    key_count = df[df["operation"] == "inserting"]["key_count"].min()
    max_time = max([time for r in random_walk_df["data"] for time in r])
    for loadfactor_group in random_walk_df.groupby("load_factor"):
        loadfactor, by_loadfactor = loadfactor_group
        fig = plot(
            by_loadfactor,
            "\n".join(
                [
                    "random walk",
                    f"inserting $10^{int(math.log10(key_count))}$ keys",
                    f"load factor = {loadfactor}",
                ]
            ) if not disabled else "",
            max_time,
            size,
        )
        path = os.path.join(output_dir, f"inserting_random_walk_lf={loadfactor}.{type}")
        fig.savefig(path, dpi=dpi)
        plt.close(fig)
        yield os.path.basename(path)

    random_walk_single_df = df[
        (df["operation"] == "inserting")
        & (df["mode"] == "random-walk")
        & (df["thread_count"] == 1)
    ]
    max_time = max([time for r in random_walk_single_df["data"] for time in r])
    fig = plot_single_thread(
        random_walk_single_df,
        "\n".join(
            [
                f"random walk inserting $10^{int(math.log10(key_count))}$ keys (single threaded)",
            ]
        ) if not disabled else "",
        max_time,
        size,
    )
    path = os.path.join(output_dir, f"inserting_random_single_threaded.{type}")
    fig.savefig(path, dpi=dpi)
    plt.close(fig)
    yield os.path.basename(path)


def render_inserting_cost_optimal(df, output_dir: str, type: str, dpi: int, size, disabled: bool):
    cost_optimal_single_df = df[
        (df["operation"] == "inserting")
        & (df["mode"] == "cost-optimal")
        & (df["thread_count"] == 1)
    ]
    assert len(df[df["operation"] == "inserting"]["key_count"].unique()) == 1
    key_count = df[df["operation"] == "inserting"]["key_count"].min()
    max_time = max([time for r in cost_optimal_single_df["data"] for time in r])
    fig = plot_single_thread(
        cost_optimal_single_df,
        f"cost optimal inserting $10^{int(math.log10(key_count))}$ keys (single threaded)" if not disabled else "",
        max_time,
        size,
    )
    path = os.path.join(output_dir, f"inserting_cost_optimal_single_threaded.{type}")
    fig.savefig(path, dpi=dpi)
    plt.close(fig)
    yield os.path.basename(path)


def render_searching_multi_threaded(df, output_dir: str, type: str, dpi: int, size, disabled: bool):
    assert len(df[df["operation"] == "inserting"]["key_count"].unique()) == 1
    key_count = df[df["operation"] == "inserting"]["key_count"].min()
    searching_df = df[(df["operation"] == "searching") & (df["thread_count"] > 1)]
    max_time = max([time for r in searching_df["data"] for time in r])
    for attrs, by_attrs in searching_df.groupby(
        ["mode", "load_factor", "search_hitting", "search_missing"]
    ):
        mode, load_factor, search_hitting, search_missing = attrs
        total = search_hitting + search_missing
        if total < key_count:
            continue
        exp = int(math.log10(total))
        hitting = round(search_hitting / total * 100)
        exp = int(math.log10(key_count))
        missing = round(search_missing / total * 100)
        fig = plot(
            by_attrs,
            "\n".join(
                [
                    f"searching $10^{exp}$ keys",
                    f"{mode} initialized table",
                    f"load factor = {load_factor}",
                    f"{hitting}% hitting / {missing}% missing",
                ]
            ) if not disabled else "",
            max_time,
            size,
        )
        path = os.path.join(
            output_dir,
            f"searching_{mode}_10^{exp}_lf={load_factor}_{hitting}_{missing}.{type}",
        )
        fig.savefig(
            path, dpi=dpi,
        )
        plt.close(fig)
        yield os.path.basename(path)


def render_searching_single_threaded(df, output_dir: str, type: str, dpi: int, size, disabled: bool):
    assert len(df[df["operation"] == "inserting"]["key_count"].unique()) == 1
    key_count = df[df["operation"] == "inserting"]["key_count"].min()
    searching_df = df[(df["operation"] == "searching") & (df["thread_count"] == 1)]
    max_time = max([time for r in searching_df["data"] for time in r])
    for attrs, by_attrs in searching_df.groupby(
        ["mode", "search_hitting", "search_missing"]
    ):
        mode, search_hitting, search_missing = attrs
        total = search_hitting + search_missing
        if total < key_count:
            continue
        exp = int(math.log10(total))
        hitting = round(search_hitting / total * 100)
        missing = round(search_missing / total * 100)
        fig = plot_single_thread(
            by_attrs,
            "\n".join(
                [
                    f"single threaded searching $10^{exp}$ keys",
                    f"{mode} initialized table",
                    f"{hitting}% hitting / {missing}% missing",
                ]
            ) if not disabled else "",
            max_time,
            size,
        )
        path = os.path.join(
            output_dir, f"searching_single_{mode}_10^{exp}_{hitting}_{missing}.{type}" 
        )
        fig.savefig(
            path, dpi=dpi, bbox_inches='tight'
        )
        plt.close(fig)
        yield os.path.basename(path)

def render_storage_modi_insert(df, output_dir: str, type: str, dpi: int, size):
    inserting_df = df[df["operation"] == "inserting"]
    max_time = max([time for r in inserting_df["data"] for time in r])
    fig = plot_storage(
        inserting_df,
        "\n".join(
            [
                f"inserting",
            ]
        ),
        max_time,
        size,
    )
    path = os.path.join(
        output_dir, f"storage_modi_insert.{type}" 
    )
    fig.savefig(
        path, dpi=dpi, bbox_inches='tight'
    )
    plt.close(fig)
    yield os.path.basename(path)

def render_storage_modi_search(df, output_dir: str, type: str, dpi: int, size):
    inserting_df = df[df["operation"] == "searching"]
    max_time = max([time for r in inserting_df["data"] for time in r])
    fig = plot_storage(
        inserting_df,
        "\n".join(
            [
                f"searching",
            ]
        ),
        max_time,
        size,
    )
    path = os.path.join(
        output_dir, f"storage_modi_search.{type}" 
    )
    fig.savefig(
        path, dpi=dpi, bbox_inches='tight'
    )
    plt.close(fig)
    yield os.path.basename(path)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Plot GeCoCoo benchmark results")
    parser.add_argument("results_dir", type=str, help="results directory")
    parser.add_argument("output_dir", type=str, help="output directory")
    parser.add_argument(
        "--op", dest="operation", choices=("inserting", "searching", "storage"), required=True
    )
    parser.add_argument(
        "--type", dest="type", choices=("png", "pdf", "pgf"), default="png"
    )
    parser.add_argument("--usetex", dest="usetex", action="store_true", default=False)
    parser.add_argument("--dpi", dest="dpi", type=int, default=300)
    parser.add_argument("--width", dest="width", type=float, default=35)
    parser.add_argument("--height", dest="height", type=float, default=20)
    parser.add_argument("--disabled", dest="disabled", type=bool, default=False, help="disable title")

    args = parser.parse_args()

    if args.usetex:
        plt.rcParams.update(
            {"pgf.rcfonts": False, "text.usetex": True, "font.family": "serif",}
        )

    print(f"loading data from {args.results_dir}...", file=sys.stderr)
    df = pd.DataFrame(collect(args.results_dir))

    if df.size == 0 and args.operation != "storage":
        print("no data!", file=sys.stderr)
        sys.exit(1)
    os.makedirs(args.output_dir, exist_ok=True)

    size = cm2inch((args.width, args.height))

    if args.operation == "inserting":
        for file in render_inserting_random_walk(
            df, args.output_dir, args.type, args.dpi, size, args.disabled
        ):
            print(file)
        for file in render_inserting_cost_optimal(
            df, args.output_dir, args.type, args.dpi, size, args.disabled
        ):
            print(file)

    if args.operation == "searching":
        for file in render_searching_multi_threaded(
            df, args.output_dir, args.type, args.dpi, size, args.disabled
        ):
            print(file)
        for file in render_searching_single_threaded(
            df, args.output_dir, args.type, args.dpi, size, args.disabled
        ):
            print(file)

    if args.operation == "storage":
        df = pd.DataFrame(collect_storage(args.results_dir))
        ordering={
            "fast": 2,
            "buckets": 1,
            "packed": 0,
        }
        df.sort_values("mode",key=(lambda a: a.map(ordering)),inplace=True)
        for file in render_storage_modi_insert(
            df, args.output_dir, args.type, args.dpi, size
        ):
            print(file)
        for file in render_storage_modi_search(
            df, args.output_dir, args.type, args.dpi, size
        ):
            print(file)

