#!/usr/bin/env python3

import os
import glob
import subprocess
from typing import List, Tuple
from dateutil.parser import isoparse
from jinja2 import Template
import datetime

GECOCOO_DIR = os.environ["GECOCOO_DIR"]


def get_benchmark_results(basepath: str):
    for commit in [f.name for f in os.scandir(basepath) if f.is_dir()]:
        commit_dir = os.path.join(basepath, commit)
        yield (commit, tuple([f.name for f in os.scandir(commit_dir) if f.is_dir()]))


def generate_histograms(results_json: str, dest_prefix: str):
    print(f"generate histograms for {results_json}")
    subprocess.run(
        ["pytest-benchmark", "compare", results_json, "--histogram", dest_prefix],
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    ).check_returncode()
    return list(map(os.path.basename, glob.glob(dest_prefix + "*")))


def generate_plots(results_dir: str, dest_dir: str):
    print("generate plots for", results_dir)
    inserting_plots = subprocess.run(
        ["python", "plot.py", results_dir, dest_dir, "--op=inserting",],
        check=True,
        capture_output=True,
        encoding="utf-8",
    ).stdout.splitlines()
    searching_plots = subprocess.run(
        ["python", "plot.py", results_dir, dest_dir, "--op=searching",],
        check=True,
        capture_output=True,
        encoding="utf-8",
    ).stdout.splitlines()

    return inserting_plots, searching_plots


def generate_benchmark_page_old(
    dest: str, reference, timestamp, diagrams: List[str], info
):
    template = Template(
        """
.. _{{ reference }}:

{{ timestamp }}
=================

..  list-table::
    :widths: 10 40

    * - **Commit**
      - {{ info.commit }}
    * - **Commit Date**
      - {{ info.commit_date }}
    * - **Author**
      - {{ info.author_name }}
    * - **Subject**
      - {{ info.subject }}
    * - **Benchmark Date**
      - {{ info.benchmark_date }}

{% for d in diagrams -%}
.. image:: {{ d}}
{% endfor %}
    
"""
    )

    with open(dest, mode="w",) as file:
        file.write(
            template.render(
                diagrams=diagrams, timestamp=timestamp, info=info, reference=reference
            )
        )


def generate_benchmark_page(
    dest: str, reference, timestamp, inserting_plots, searching_plots, info
):
    template = Template(
        """
.. _{{ reference }}:

{{ timestamp }}
=================

..  list-table::
    :widths: 10 40

    * - **Commit**
      - {{ info.commit }}
    * - **Commit Date**
      - {{ info.commit_date }}
    * - **Author**
      - {{ info.author_name }}
    * - **Subject**
      - {{ info.subject }}
    * - **Benchmark Date**
      - {{ info.benchmark_date }}

Inserting
---------

{% for p in inserting_plots -%}
.. image:: {{ p }}
{% endfor %}

Searching
---------

{% for p in searching_plots -%}
.. image:: {{ p }}
{% endfor %}

    
"""
    )

    with open(dest, mode="w",) as file:
        file.write(
            template.render(
                inserting_plots=inserting_plots,
                searching_plots=searching_plots,
                timestamp=timestamp,
                info=info,
                reference=reference,
            )
        )


def generate_commit_page(
    dest: str, commit: str, short_commit: str, timestamps: List[str]
):
    template = Template(
        """        
{{ short_commit }}
=================

..  toctree::
    :maxdepth: 3

    {% for ts in timestamps -%}
    {{ ts }}/index
    {% endfor %}
    
"""
    )

    with open(dest, mode="w",) as file:
        file.write(
            template.render(
                commit=commit, short_commit=short_commit, timestamps=timestamps
            )
        )


def generate_summary_page(results: List[Tuple[str, str]], commit_infos):
    template = Template(
        """
GeCoCoo Benchmarks
==================

..  toctree::
    :maxdepth: 3
    :caption: Benchmarks:
    :hidden:

    {% for commit, timestamps in results -%}
    {{ commit }}/index
    {% endfor %}

   
..  list-table::
    :widths: 15 10 15 50
    :header-rows: 1

    * - Commit
      - Author
      - Date
      - Benchmarks
    {%- for commit, timestamps in results %}
    * - {{ commit_infos[commit].short_commit }}
      - {{ commit_infos[commit].author_name }}
      - {{ commit_infos[commit].commit_date }}
      - {% for ts in timestamps -%}
        - :ref:`{{ commit ~ "_" ~ ts | replace(":", "_") }}`
        {% endfor %}
    {%- endfor -%}
    
"""
    )

    with open("pages/source/index.rst", "w") as file:
        file.write(template.render(results=results, commit_infos=commit_infos))


def git_info(commit: str):
    lines = subprocess.run(
        ["git", "rev-list", "--format=%h\n%an\n%aI\n%s", "--max-count=1", commit],
        cwd=GECOCOO_DIR,
        check=True,
        capture_output=True,
        encoding="utf-8",
    ).stdout.splitlines()

    return dict(
        short_commit=lines[1],
        author_name=lines[2],
        commit_date=datetime.datetime.fromisoformat(lines[3]),
        subject=lines[4],
    )


if __name__ == "__main__":
    base = "results"
    pages_base = "pages/source/"
    results = list(get_benchmark_results(base))
    commit_infos = {}
    for commit, timestamps in results:
        commit_info = git_info(commit)
        commit_infos[commit] = commit_info
        for ts in timestamps:
            parsed_time = datetime.datetime.fromisoformat(ts.replace("_", ":"))
            benchmark_results = os.path.join(base, commit, ts)
            benchmark_pages = os.path.join(pages_base, commit, ts)
            old = os.access(os.path.join(benchmark_results, "results.json"), os.R_OK)
            os.makedirs(benchmark_pages, exist_ok=True)
            if old:
                diagrams = generate_histograms(
                    os.path.join(benchmark_results, "results.json"),
                    os.path.join(benchmark_pages, "histogram"),
                )
                generate_benchmark_page_old(
                    os.path.join(benchmark_pages, "index.rst"),
                    f"{commit}_{ts}".replace(":", "_"),
                    parsed_time,
                    diagrams,
                    dict(commit=commit, benchmark_date=parsed_time, **commit_info),
                )
            else:
                inserting_plots, searching_plots = generate_plots(
                    os.path.join(benchmark_results, "results"), benchmark_pages
                )
                generate_benchmark_page(
                    os.path.join(benchmark_pages, "index.rst"),
                    f"{commit}_{ts}".replace(":", "_"),
                    parsed_time,
                    inserting_plots,
                    searching_plots,
                    dict(commit=commit, benchmark_date=parsed_time, **commit_info),
                )

        generate_commit_page(
            os.path.join(pages_base, commit, "index.rst"),
            commit,
            commit_info["short_commit"],
            timestamps,
        )

    generate_summary_page(
        sorted(results, key=lambda x: commit_infos[x[0]]["commit_date"], reverse=True),
        commit_infos,
    )
