from gecocoo.config import (
    Config,
    HashFunction,
    StorageConfig,
    StorageMode,
    ValueModuleConfig,
)
from gecocoo.hashing import xorlinear
from gecocoo.helper import get_qualified_name
from gecocoo.valuemodules import counting
from gecocoo.config.lowlevel import LLConfigJSONEncoder, resolve_config

import os
import json

BUCKET_SIZES = [2, 4, 6, 8, 10]
KS = [31]
STORAGE_MODES = [StorageMode.BUCKETS]  # , StorageMode.PACKED, StorageMode.FAST]
LOAD_FACTORS = [0.75, 0.95, 0.97]
HASH_FUNCTION_COUNTS = [2, 3, 4]
N = 10 ** 8
CONFIGS = list(
    [
        Config(
            bucket_size=bucket_size,
            k=k,
            n=N,
            load_factor=load_factor,
            value_module=ValueModuleConfig(module_name=get_qualified_name(counting)),
            hash_functions=tuple(
                [
                    HashFunction(name=get_qualified_name(xorlinear))
                    for _ in range(hash_function_count)
                ]
            ),
            storage=StorageConfig(
                mode=storage_mode,
                concurrent=True,
                disable_quotienting=storage_mode == StorageMode.FAST,
            ),
        )
        for bucket_size in BUCKET_SIZES
        for k in KS
        for storage_mode in STORAGE_MODES
        for load_factor in LOAD_FACTORS
        for hash_function_count in HASH_FUNCTION_COUNTS
    ]
)

# some benchmarks take longer than 2 hrs so we need to run them on another LiDO3 partition
LIDO3_DEFAULT_PARTITION = "short"
LIDO3_PARTITIONS = {
    "benchmark_cost_optimal": {
        "config_3": "med",
        "config_7": "med",
    },
}


localrules:
    generate_config,


rule all:
    input:
        [
            f"results/config_{i}-{file}"
            for file in [
                "results-random-walk.json",
                "cpu-random-walk.txt",
                "results-cost-optimal.json",
                "cpu-cost-optimal.txt",
            ]
            for i in range(len(CONFIGS))
        ],


rule generate_config:
    output:
        "generated-configs/config_{i}.json",
    run:
        llconfig = resolve_config(CONFIGS[int(wildcards.i)])
        os.makedirs(os.path.dirname(output[0]), exist_ok=True)
        with open(output[0], mode="w") as file:
            json.dump(llconfig, file, cls=LLConfigJSONEncoder, indent=4)


rule benchmark_random_walk:
    input:
        "generated-configs/{config}.json",
    output:
        json="results/{config}-results-random-walk.json",
        cpu_info="results/{config}-cpu-random-walk.txt",
    log:
        "logs/{config}-random-walk.log",
    resources:
        mem_mb=32000,
    threads: 16
    shell:
        """
        lscpu > {output.cpu_info}

        export GECOCOO_CONFIG={input}
        export WARMUP_ROUNDS=1
        export INSERT_ROUNDS=10
        export SEARCH_ROUNDS=10

        .venv/bin/pytest bench_operations.py::test_random_walk -s --benchmark-json={output.json} --benchmark-save-data 2>{log} 1>{log}
        """


rule benchmark_cost_optimal:
    input:
        "generated-configs/{config}.json",
    output:
        json="results/{config}-results-cost-optimal.json",
        cpu_info="results/{config}-cpu-cost-optimal.txt",
    params:
        partition=lambda wildcards: LIDO3_PARTITIONS["benchmark_cost_optimal"].get(wildcards.config, "short")
    log:
        "logs/{config}-cost-optimal.log",
    resources:
        mem_mb=32000,
    threads: 16
    shell:
        """
        lscpu > {output.cpu_info}

        export GECOCOO_CONFIG={input}
        export WARMUP_ROUNDS=0
        export INSERT_ROUNDS=1
        export SEARCH_ROUNDS=5

        .venv/bin/pytest bench_operations.py::test_cost_optimal -s --benchmark-json={output.json} --benchmark-save-data 2>{log} 1>{log}
        """
