from threading import Thread
from numba.core.utils import benchmark

import numpy as np
from gecocoo import config
from typing import Callable, List, Tuple
from gecocoo.interface import HashtableInterface, HashtableUtilities
import json
from pytest_benchmark.fixture import BenchmarkFixture

from gecocoo.config.lowlevel import (
    LLConfig,
    LLConfigJSONEncoder,
    from_json,
    resolve_config,
)
from gecocoo.generator import generate_hashtable
from gecocoo.helper import generate_random_kmers, get_qualified_name
from gecocoo.valuemodules.default import keyset
import os
import pytest
from numba import njit, uint64


def create_benchmark(request):
    bs = request.config._benchmarksession

    if bs.skip:
        pytest.skip("Benchmarks are skipped (--benchmark-skip was used).")
    else:
        node = request.node
        marker = node.get_closest_marker("benchmark")
        options = dict(marker.kwargs) if marker else {}
        if "timer" in options:
            options["timer"] = NameWrapper(options["timer"])
        fixture = BenchmarkFixture(
            node,
            add_stats=bs.benchmarks.append,
            logger=bs.logger,
            warner=request.node.warn,
            disabled=bs.disabled,
            **dict(bs.options, **options),
        )
        request.addfinalizer(fixture._cleanup)
        return fixture


@pytest.fixture(scope="function")
def benchmark_creator(request):
    return lambda: create_benchmark(request)


@pytest.fixture(scope="function")
def bench_config(request):
    config_file = os.environ["GECOCOO_CONFIG"]
    with open(config_file) as file:
        llconfig = json.load(file, object_hook=from_json)

    return llconfig


INSERT_ROUNDS = int(os.environ["INSERT_ROUNDS"])
SEARCH_ROUNDS = int(os.environ["SEARCH_ROUNDS"])
WARMUP_ROUNDS = int(os.environ["WARMUP_ROUNDS"])

SEED = 23234234
KEY_COUNT = 10 ** 8
SEARCH_KEY_COUNTS = [10 ** 6, 10 ** 7, 10 ** 8]
THREAD_COUNTS = [1, 4, 8, 16]


def benchmark_insert(
    benchmark: BenchmarkFixture,
    bench_config: LLConfig,
    use_cost_optimal: bool,
    thread_count: int,
):
    table_count = INSERT_ROUNDS + WARMUP_ROUNDS
    print(f"generating {table_count} tables")
    tables = list(
        [generate_hashtable(bench_config) for _ in range(INSERT_ROUNDS + WARMUP_ROUNDS)]
    )
    keys = generate_random_kmers(bench_config.k, KEY_COUNT, SEED, None)
    current_insert = 0

    def setup():
        nonlocal current_insert
        table, _ = tables[current_insert]
        current_insert += 1

        print(f"benchmarking insert {current_insert}/{len(tables)}")

        if use_cost_optimal:
            return (table, [keys]), {}
        else:
            return (table, np.array_split(keys, thread_count)), {}

    if use_cost_optimal:

        def insert(table: HashtableInterface, keys):
            table.initialize(table.storage, keys)

    else:

        @njit(nogil=True)
        def insert(table: HashtableInterface, keys):
            for k in keys:
                table.insert_single(table.storage, k)

    def work(table: HashtableInterface, keys_chunks):
        threads = list(
            [
                Thread(target=insert, args=(table, keys_chunk))
                for keys_chunk in keys_chunks
            ]
        )
        for t in threads:
            t.start()
        for t in threads:
            t.join()

    benchmark.group = f"inserting"
    benchmark.name = f"insert"
    benchmark.extra_info["key_count"] = KEY_COUNT
    benchmark.extra_info["thread_count"] = thread_count
    benchmark.extra_info["config"] = LLConfigJSONEncoder().encode(bench_config)
    benchmark.pedantic(
        work, setup=setup, rounds=INSERT_ROUNDS, warmup_rounds=WARMUP_ROUNDS,
    )

    return tables, keys


def benchmark_search(
    benchmark: BenchmarkFixture,
    bench_config: LLConfig,
    tables: List[Tuple[HashtableInterface, HashtableUtilities]],
    existing_keys,
    thread_count: int,
    key_missing_count: int,
    key_hitting_count: int,
):
    current_search = 0
    search_keys = np.concatenate(
        [
            np.random.choice(existing_keys, key_hitting_count),
            np.random.randint(
                0, 4 ** bench_config.k, key_missing_count, dtype=np.uint64
            ),
        ]
    )
    np.random.shuffle(search_keys)

    def search():
        nonlocal current_search
        table, _ = tables[current_search % len(tables)]
        current_search += 1

        print(
            f"benchmarking search {current_search}/{len(tables)} ({key_missing_count + key_hitting_count} keys)"
        )

        return (table, np.array_split(search_keys, thread_count)), {}

    @njit(nogil=True)
    def search_chunk(table: HashtableInterface, keys):
        sum = uint64(0)
        for k in keys:
            result = table.search_single(table.storage, k)
            if result is not None:
                sum += result
        return sum

    def work(table: HashtableInterface, chunks):
        threads = list(
            [Thread(target=search_chunk, args=(table, chunk)) for chunk in chunks]
        )
        for t in threads:
            t.start()
        for t in threads:
            t.join()

    benchmark.group = "searching"
    benchmark.name = "search"
    benchmark.extra_info["key_count"] = KEY_COUNT
    benchmark.extra_info["search_missing"] = key_missing_count
    benchmark.extra_info["search_hitting"] = key_hitting_count
    benchmark.extra_info["thread_count"] = thread_count
    benchmark.extra_info["config"] = LLConfigJSONEncoder().encode(bench_config)
    benchmark.pedantic(
        work, setup=search, rounds=SEARCH_ROUNDS, warmup_rounds=WARMUP_ROUNDS,
    )


@pytest.mark.parametrize("thread_count", THREAD_COUNTS)
def test_random_walk(
    benchmark_creator, bench_config: LLConfig, thread_count: int, reraise,
):
    print(f"benchmarking with {thread_count} thread(s)")
    tables, keys = benchmark_insert(
        benchmark_creator(), bench_config, False, thread_count
    )

    for search_key_count in SEARCH_KEY_COUNTS:
        benchmark_search(
            benchmark_creator(),
            bench_config,
            tables,
            keys,
            thread_count,
            0,
            search_key_count,
        )
        benchmark_search(
            benchmark_creator(),
            bench_config,
            tables,
            keys,
            thread_count,
            search_key_count,
            0,
        )
        benchmark_search(
            benchmark_creator(),
            bench_config,
            tables,
            keys,
            thread_count,
            search_key_count // 2,
            search_key_count // 2,
        )


def test_cost_optimal(
    benchmark_creator, bench_config: LLConfig, reraise,
):
    tables, keys = benchmark_insert(
        benchmark_creator(), bench_config, use_cost_optimal=True, thread_count=1
    )

    for thread_count in THREAD_COUNTS:
        for search_key_count in SEARCH_KEY_COUNTS:
            benchmark_search(
                benchmark_creator(),
                bench_config,
                tables,
                keys,
                thread_count,
                0,
                search_key_count,
            )
            benchmark_search(
                benchmark_creator(),
                bench_config,
                tables,
                keys,
                thread_count,
                search_key_count,
                0,
            )
            # benchmark_search(
            #     benchmark_creator(),
            #     bench_config,
            #     tables,
            #     keys,
            #     thread_count,
            #     search_key_count // 2,
            #     search_key_count // 2,
            # )

